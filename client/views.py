import django_tables2 as tables
import slumber
from django import forms
from django.http import HttpResponse
from django.shortcuts import render, redirect


class ProbaTable(tables.Table):
    id = tables.Column()
    descriere = tables.Column()
    id_arbitru = tables.Column()
    delete = tables.TemplateColumn(template_name='delete.html')

    class Meta:
        template_name = 'django_tables2/bootstrap.html'


class ProbaForm(forms.Form):
    id = forms.IntegerField(required=True)
    descriere = forms.CharField(max_length=30, required=True)
    id_arbitru = forms.IntegerField(required=True)


def delete(request, pk):
    api = slumber.API("http://localhost:8080/triatlon/")
    api.probe(pk).delete()
    return redirect('index')


def index(request):
    api = slumber.API("http://localhost:8080/triatlon/")
    data = api.probe.get()
    table = ProbaTable(data)

    if request.method == "POST":
        form = ProbaForm(request.POST)
        if form.is_valid():
            id = form.cleaned_data['id']
            descriere = form.cleaned_data['descriere']
            id_arbitru = form.cleaned_data['id_arbitru']

            proba = {'id': id, 'descriere': descriere}
            api.arbitrii(id_arbitru).probe.post(proba)

            return redirect('index')
    else:
        form = ProbaForm()

    return render(request, 'index.html', {'table': table, 'form': form})
